#!/usr/bin/python3
#-*- coding: utf-8 -*-

"""
@author Sebastian Keck
@date 2020-02-23

Calculator for basic mathematical operations.
"""

import sys

def read_from_command_line(text_to_show: str) -> int:
    """
    Reads text from user input.
    @param text_to_show: Text that's shown at user input
    @return int
    """

    text = input(text_to_show)
    return int(text)

def calculate(operation_type: int, first_number: int, second_number: int) -> float:
    # Fill with the different basic calculation types:
    # add, subtract, multiply, divide
    # TODO: do the calculations
    if operation_type == 1:
        return first_number + second_number
    elif operation_type == 2:
        return first_number - second_number
    elif operation_type == 3:
        return first_number * second_number
    else:
        if second_number == 0:
            print("Teilen durch Null nicht möglich")
            return None
        else: 
            return first_number / second_number


if __name__ == '__main__':
    # TODO: Get type of operation
    # Hint: multiline string with different options (1-4)
    operation_type="Bitte wählen Sie eine Rechenart aus und geben Sie die Zahl dahinter an: \n 1 = Addition \n 2 = Subtraktion \n 3 = Multiplikation \n 4 = Division: "
    Rechenart = read_from_command_line(operation_type)
    # TODO: Get user input for the two numbers
    Zahl1 = read_from_command_line("Geben Sie Ihre erste Zahl ein: ")
    Zahl2 = read_from_command_line("Geben Sie Ihre zweite Zahl ein: ")
    # TODO: Output the result at the terminal to inform the user
    print(calculate(Rechenart, Zahl1, Zahl2))

    